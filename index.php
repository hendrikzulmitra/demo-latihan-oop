<?php


require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$animal = new animal("shaun");
echo "Name = " . $animal->nama . "<br>";
echo "Legs = " . $animal->legs . "<br>";
echo "Cold Blooded = " . $animal->coldblooded . "<br><br>";

$frog = new frog("buduk");
echo "Name = " . $frog->nama . "<br>";
echo "Legs = " . $frog->legs . "<br>";
echo "Cold Blooded = " . $frog->coldblooded . "<br>";
$frog->jump("Hop Hop"). "<br><br>";
echo "<br><br>";


$ape = new ape("Kera Sakti");
echo "Name = " . $ape->nama . "<br>";
echo "Legs = " . $ape->legs . "<br>";
echo "Cold Blooded = " . $ape->coldblooded . "<br>";
$ape->bunyi("Auoooo");




?>